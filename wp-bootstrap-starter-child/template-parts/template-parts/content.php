<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="post-thumbnail">
		<?php the_post_thumbnail(); ?>
	</div>
	<header class="entry-header">		
	<?php
		if ( is_single() ) :
			the_title( '<h1 class="receipe">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			
			<?php wp_bootstrap_starter_posted_on(); ?>
			
	|  	Categorie: <?php echo get_the_category_list( ', ' ); ?> 

			
			| Beredingstijd: <span class="time"><?php the_field('preparation_time'); ?></span>			
			
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->
	
	<div class="summary">
		<?php the_content( 'Read more ...' ); ?>
	</div>

	<br>

	<div class="row">
	<div class="col-sm-12 col-lg-7">
		<div class="entry-content">
<h2><?php the_field('in_advance_title'); ?></h2>
<?php the_field('in_advance'); ?>
<h2><?php the_field('preparation_title'); ?></h2>
<?php the_field('preparation'); ?>
<h2><?php the_field('serving_tips_title'); ?></h2>
<?php the_field('serving_tips'); ?>
		</div>
	</div><div class="col-sm-12 col-lg-5">	
		<?php get_template_part( 'template-parts/ingredients', get_post_format() ); ?>
		</div>	
	</div>	