<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<div class="col-sm-6"><div class="index-summary">

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="post-thumbnail">
		<?php the_post_thumbnail(); ?>
	</div>
	<header class="entry-header">		
	<?php
		if ( is_single() ) :
			the_title( '<h1 class="receipe">', '</h1>' );
		else :
			the_title( '<h2><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
		
				<p class="details">			
	 	Categorie: <?php echo get_the_category_list( ', ' ); ?> 

			
			| Beredingstijd: <span class="time"><?php the_field('preparation_time'); ?></span>			
			</p>
			
			<?php the_content(); ?>
			
		
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
		</div></div>
	
	</header><!-- .entry-header -->